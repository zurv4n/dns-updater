This DNS Updater script works with curl and jq to compare a server's current ip address with DNS records for a domain or domains.
Once checked, it there is a mismatch between the A record and the current server IP, it will replace the A name record with the current IP.
This was created for self-hosted servers behind a dynamic IP.