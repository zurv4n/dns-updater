#!/bin/bash

#IP Check Script
#This script will cycle through Cloudflare registered domains and update their DNS entries to a server's current IP
#Meant for use with server's behind a dynamic IP ISP
#The script will pull current server IP, then compare to DNS entry for each IP listed in 'domains'
#Outputs to shell for record keeping. Comment for silent output

#Requirements:
#Curl
#jq library

#Insert Domain Identifiers as found in Cloudflare
domains=('identifier_1' 'identifier_2' 'identifier_3')

echo "Checking current IP at site." 
ipCur=$(curl -s https://api.ipify.org)
echo "Current IP address for site is: $ipCur"

for dName in "${domains[@]}"
do
	echo "Pulling current IP in DNS record, please wait."
	dnsResult=$(curl -s --request GET \
		--url https://api.cloudflare.com/client/v4/zones/"$dName"/dns_records?type="A" \
		-H 'Content-Type: application/json' \
		-H 'Authorization: Bearer INSERT_AUTH_TOKEN')
	dnsIP=$(jq -r '.result[0] .content' <<< "$dnsResult")
	dnsName=$(jq -r '.result[0] .name' <<< "$dnsResult")
	ident=$(jq -r '.result[0] .id' <<< "$dnsResult")
	echo "Current DNS record for $dnsName is: $dnsIP"
	#echo "$ident"
	if [ "$ipCur" == "$dnsIP" ]
	then
		echo "DNS IP and site IP are a match."
	else
		echo "IP mismatch: Records updating."
		$(curl -s --request PATCH \
			--url https://api.cloudflare.com/client/v4/zones/"$dName"/dns_records/"$ident" \
			-H 'Content-Type: application/json' \
			-H 'Authorization: Bearer INSERT_AUTH_TOKEN' \
			-d '{
			"content": "'"$ipCur"'",
			"name":"@",
			"ttl": 1
			}'>/dev/null)
		echo "Records for $dnsName have been updated."
	fi
done
echo "All records are correct."